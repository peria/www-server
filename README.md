# WWW server
The server code for `www.tiagosimao.com`

Make sure you have a `.env` file in the project root with the following contents:
```sh
PUBLIC_HOSTNAME=www.example.com

DOUBT53_GANDI_API_TOKEN=...
DOUBT53_ZONE=example.com
DOUBT53_A_RECORD_NAME=www

WEBCABLE_ROUTER_ADDRESS=192.168.1.254
WEBCABLE_ROUTER_USER=...
WEBCABLE_ROUTER_PASS=...

KAMINO_REMOTE_URL=https://codeberg.org/tiagosimao/www.tiagosimao.com.git
```

You can use `PUBLIC_HOSTNAME=localhost` for local development (self-signed cert used)

Run the whole thing with `make up`

# Raspberryos init
```sh
curl --proto '=https' --tlsv1.2 -sSf https://codeberg.org/peria/www-server/raw/branch/main/init.sh | sh
```