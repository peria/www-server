#!/bin/sh
if which nslookup > /dev/null; then
  nslookup `hostname` | grep -i address | awk -F" " '{print $2}' | awk -F# '{print $1}' | tail -n 1
else
  hostname -I | awk '{print $1;}'
fi