#!/bin/sh
set -eux

sudo apt-get update -y

sudo apt install -y unattended-upgrades curl gnupg

if [ ! -f /etc/apt/keyrings/docker.gpg ]; then
    # Add Docker's official GPG key:
    sudo install -m 0755 -d /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    sudo chmod a+r /etc/apt/keyrings/docker.gpg

    # Add the repository to Apt sources:
    echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
    $(. /etc/os-release && echo "bullseye") stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt-get update -y
fi

if ! which docker > /dev/null; then
    sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
fi

if ! id -nG "$USER" | grep -qw "docker"; then
    sudo usermod -aG docker $USER
fi

if [ ! -d "${HOME}/www-server" ]; then
    git clone https://codeberg.org/peria/www-server.git
fi

# todo
# create .env file
# start service
# setup reconcile process

#updatecmd="cd ${HOME}/www-server && make reconcile > ${HOME}/www-server/reconcile.log 2>&1"
#cronjob="* * * * * $updatecmd"
#( crontab -l | grep -v -F "$updatecmd" || true ; echo "$cronjob" ) | crontab -
