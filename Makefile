up: down
	export WEBCABLE_LOCAL_IPV4=`./detect_local_ip.sh` && docker compose up -d --remove-orphans

down:
	docker compose down --remove-orphans

update:
	git pull

reload:
	docker compose restart

reconcile: update reload
	echo "www-server reconciled"